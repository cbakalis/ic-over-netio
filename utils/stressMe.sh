#!/bin/env sh
for index in {1..2500}
do
    echo "RETRY: $index"
    if [[ $1 == bb5 ]] || [[ $1 == vs ]]; then
	if [[ $2 == debug ]]; then
	    ../build/multi_gbtx_ic -c ../cfg_$1.json -d
	elif  [[ $2 == trace ]]; then
	    ../build/multi_gbtx_ic -c ../cfg_$1.json -t
	else
	    ../build/multi_gbtx_ic -c ../cfg_$1.json
	fi
	if [[ $? == 20 ]]; then
	    break
	fi
    else
	echo "please pass argument bb5 or vs, and then choose if you want debug or not E.g. : $ source stressMe.sh bb5 debug or $ source stressMe.sh bb5"
    fi
    sleep 0.2
done


#ifndef _NSW_IC_LOGGING_
#define _NSW_IC_LOGGING_

#include "spdlog/spdlog.h"

extern std::shared_ptr<spdlog::logger> console;

#endif

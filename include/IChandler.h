

#ifndef __IC_HANDLER__
#define __IC_HANDLER__

#include <string>
#include <vector>
#include <cstdint>

#include "connectionHandler.h"

#include <netio.hpp>
#include <felixbase/client.hpp>

class IChandler
{
  public:
    IChandler() = delete;
    IChandler(ConnectionHandler&, std::string flxHost, unsigned int portToGBTx, unsigned int portFromGBTx, uint32_t elinkID, bool resubscribeValue = false);
    ~IChandler();

    void sendCfg(std::string);
    void sendRegs(std::map<uint16_t, std::vector<uint8_t>>);
    void setMaxRetries(int);
    bool isConnected();

    std::vector<uint8_t>                      readCfg();
    std::vector<std::pair<uint16_t, uint8_t>> readRegs(std::vector<uint16_t>);

  private:
    ConnectionHandler& m_connHandler;
    int m_maxRetries;
    bool m_resubscribe;
    std::string m_flxHost;
    unsigned int m_portToGBTx;
    unsigned int m_portFromGBTx;
    uint32_t m_elinkID;

    void resubscribe();

  protected:
};

#endif

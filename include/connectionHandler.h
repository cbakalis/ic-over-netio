
#ifndef __CONNECTION_HANDLER__
#define __CONNECTION_HANDLER__

#include <string>
#include <vector>
#include <map>
#include <cstdint>
#include <memory>
#include <thread>
#include <mutex>
#include <condition_variable>

#include <netio.hpp>
#include <felixbase/client.hpp>

class ICSocket;
class ICRequestSocket;
class ICReplySocket;

struct ConnectionInfo
{
  std::string flxHost;
  unsigned int portToGBTx;
  unsigned int portFromGBTx;
  uint32_t elinkID;

  std::shared_ptr<ICRequestSocket> requestSocket;
  std::shared_ptr<ICReplySocket>   replySocket;
};

class ICSocket
{
  public:
    ICSocket() = delete;
    ICSocket(netio::context *, std::string);
    virtual ~ICSocket();

    // Delete Copy and Assignment
    ICSocket(const ICSocket&) = delete;
    ICSocket& operator=(const ICSocket&) = delete;

    virtual void free() = 0;
    virtual void connect() = 0;
    void reconnect()
    {
      free();
      connect();
    };

    std::string flxHost() {return m_flxHost;};

  private:
  protected:
    netio::context* m_netioContext;
    std::string m_flxHost;
};

class ICRequestSocket: public ICSocket
{
  public:
    ICRequestSocket() = delete;
    ICRequestSocket(netio::context *, std::string, unsigned int);
    ~ICRequestSocket();

    // Delete Copy and Assignment
    ICRequestSocket(const ICRequestSocket&) = delete;
    ICRequestSocket& operator=(const ICRequestSocket&) = delete;

    void free();
    void connect();

    bool connected() {return m_connected;};

    void sendNetioMessage(std::vector<unsigned char>&, std::shared_ptr<ICReplySocket>, int maxAttempts = 3, bool resubscribeOnRetry = false);

    unsigned int portToGBTx() {return m_portToGBTx;};

  private:
    unsigned int m_portToGBTx;

    netio::low_latency_send_socket m_netioRequestSocket;
    bool m_connected{false};

  protected:
};

class ICReplySocket: public ICSocket
{
  public:
    ICReplySocket() = delete;
    ICReplySocket(netio::context *, std::string, unsigned int, uint32_t);
    ~ICReplySocket();

    // Delete Copy and Assignment
    ICReplySocket(const ICReplySocket&) = delete;
    ICReplySocket& operator=(const ICReplySocket&) = delete;

    void free();
    void connect();

    void replyHandler(netio::endpoint&, netio::message&);

    bool subscribed() {return m_subscribed;};

    void resetReply() {m_hasReply = false;};
    volatile bool hasReply() {return m_hasReply;};
    std::vector<uint8_t> getReply() {if(m_hasReply) return m_reply; return {};};

    std::mutex&                  mtx() {return m_mtx;};
    std::condition_variable& condVar() {return m_condVar;};

    unsigned int portFromGBTx() {return m_portFromGBTx;};
    uint32_t          elinkID() {return m_elinkID;};

  private:
    unsigned int m_portFromGBTx;
    uint32_t     m_elinkID;

    netio::low_latency_subscribe_socket m_netioReplySocket;
    bool m_subscribed{false};

    std::mutex m_mtx;
    std::condition_variable m_condVar;

    // Set variable as volatile because it is being changed in one thread and accessed in another
    volatile bool m_hasReply{false};
    std::vector<uint8_t> m_reply{};

  protected:
};

class ConnectionHandler
{
  public:
    ConnectionHandler();
    ~ConnectionHandler();

    void netioMainThread(void);

    void freeConnections(void);

    ConnectionInfo getConnection(std::string, unsigned int, unsigned int, uint32_t);

  private:
    netio::context m_netioContext;
    std::thread m_netioThread;

    std::map<std::string,std::shared_ptr<ICRequestSocket>> m_requestSockets;
    std::map<std::string,std::shared_ptr<ICReplySocket>>   m_replySockets;

    void freeReplySockets(void);
    void freeRequestSockets(void);

  protected:
};

#endif

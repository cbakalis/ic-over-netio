/*****************************************************************************/
/*     readIcCfg.h                                                           */
/*                                                                           */
/*   Header file for the class that will read the IC config file and then    */
/* have several methods to return this data, in particular as a netio frame  */
/*                                                                           */
/*  Authors:                                                                 */
/*      Christos Bakalis                                                     */
/*      Cristovao da Cruz e Silva                                            */
/*****************************************************************************/


#ifndef _NSW_READ_IC_CFG_
#define _NSW_READ_IC_CFG_

#include <string>
#include <vector>
#include <cstdint>

class readICCfg
{
  public:
    readICCfg() = delete;
    readICCfg(std::string);

    const std::vector<uint8_t>  data() const {return m_data;}
          std::vector<uint8_t>& data()       {return m_data;}
    const uint8_t  data(size_t i) const {return m_data[i];}
          uint8_t& data(size_t i)       {return m_data[i];}
    uint8_t* dataArray() {return m_data.data();}

    std::vector<uint8_t> getNetioFrame(bool isRead = false) const;

  private:
  protected:
    std::string m_cfg_file_name;

    bool m_isOK{false};
    std::vector<uint8_t> m_data;

};

#endif

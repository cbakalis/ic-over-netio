#ifndef __UTILITIES__
#define __UTILITIES__

#include<vector>
#include<string>
#include<cstdint>

bool isFile(std::string filename);
std::vector<uint8_t> prepareNetioFrame(bool read, uint16_t startAddr, std::vector<uint8_t>& data, uint8_t i2cAddr = 1);

#endif

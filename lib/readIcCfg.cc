#include "readIcCfg.h"
#include "logging.h"
#include "utilities.h"

#include <fstream>
#include <cstdlib>
#include <stdexcept>
#include <cstdint>

uint8_t strTouint8(std::string str)
{
  if(str.size() > 2 || str.size() == 0)
    throw std::logic_error("\"" + str + "\" is not a valid uint_8");

  uint8_t retVal = 0;

  for(size_t i = 0; i < str.size(); ++i)
  {
    retVal = retVal << 4;
    switch(str[i])
    {
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
        retVal += static_cast<uint8_t>(str[i] - '0');
        break;
      case 'a':
      case 'b':
      case 'c':
      case 'd':
      case 'e':
      case 'f':
        retVal += static_cast<uint8_t>(str[i] - 'a') + 10;
        break;
      default:
        throw std::out_of_range("The value " + str + " is  not valid hexadecimal");
    }
  }

  return retVal;
}

readICCfg::readICCfg(std::string cfg_file):
  m_cfg_file_name(cfg_file)
{
  if(!isFile(cfg_file))
  {
    console->error("The given file ({:s}) does not exist", m_cfg_file_name);
    throw std::logic_error("File does not exist");
  }

  m_data.reserve(1024);
  std::ifstream inFile(m_cfg_file_name);

  std::string line;
  m_isOK = true;

  try{
  while(std::getline(inFile, line))
  {
    if(line.size() > 2 || line.size() == 0)
    {
      console->error("File {:s} does not have the correct format", m_cfg_file_name);
      m_isOK = false;
      break;
    }

    m_data.push_back(strTouint8(line));

    if(m_data.size() > 1023)
    {
      console->error("File {:s} has too many lines", m_cfg_file_name);
      m_isOK = false;
      break;
    }
  }
  }
  catch(std::logic_error& e)
  {
    console->error("File {:s} does not have the correct format, message: {:s}", m_cfg_file_name, e.what());
    m_isOK = false;
  }
}

std::vector<uint8_t> readICCfg::getNetioFrame(bool isRead) const
{
  std::vector<uint8_t> retVal = {};

  if(!m_isOK)
    throw std::logic_error("The data is not initialized");

  constexpr size_t header_size = 7;
  constexpr size_t footer_size = 2;
  // TODO: Should we add the delimiters top and bottom?
  retVal.reserve(header_size + m_data.size() + footer_size);

  retVal.push_back(0); // Delimiter
  retVal.push_back((0x01 << 1) + (isRead?0x1:0x0)); // GBTX I2C address and read/write bit
  retVal.push_back(1); // Command (not used in GBTX v1 + 2)
  retVal.push_back(m_data.size() & 0xFF); // Number of data bytes
  retVal.push_back(m_data.size() >> 8);
  retVal.push_back(0); // Register (start) address
  retVal.push_back(0);

  for(auto& val: m_data)
    retVal.push_back(val);

  uint8_t parity = 0;
  for(size_t i = 2;i < retVal.size(); ++i)
    parity ^= retVal[i];
  retVal.push_back(parity);
  retVal.push_back(0); // Delimiter

  return std::move(retVal);
}

#include "utilities.h"
#include "logging.h"

#include <sys/stat.h>
#include <cstdint>

bool isFile(std::string fileName)
{
  struct stat buffer;
  return (stat(fileName.c_str(), &buffer) == 0);
}

std::vector<uint8_t> prepareNetioFrame(bool read, uint16_t startAddr, std::vector<uint8_t>& data, uint8_t i2cAddr)
{
  std::vector<uint8_t> retVal = {};

  constexpr size_t header_size = 7;
  constexpr size_t footer_size = 2;

  // TODO: Should we add the delimiters top and bottom?
  if(!read)
    retVal.reserve(header_size + data.size() + footer_size);
  else
    retVal.reserve(header_size + footer_size);

  retVal.push_back(0); // Delimiter
  retVal.push_back((0x01 << 1) + (read?0x1:0x0)); // GBTX I2C address and read/write bit
  retVal.push_back(1); // Command (not used in GBTX v1 + 2)
  retVal.push_back(data.size() & 0xFF); // Number of data bytes
  retVal.push_back((data.size() >> 8) & 0xFF);
  retVal.push_back(startAddr   & 0xFF); // Register (start) address
  retVal.push_back(startAddr >> 8);

  if(!read)
    for(auto& val: data)
      retVal.push_back(val);

  uint8_t parity = 0;
  for(size_t i = 2;i < retVal.size(); ++i)
    parity ^= retVal[i];
  retVal.push_back(parity);
  //retVal.push_back(0); // Delimiter

  return std::move(retVal);
}


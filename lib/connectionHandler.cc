#include "logging.h"
#include "connectionHandler.h"
#include <chrono>
#include <exception>

using namespace std::chrono_literals;

std::chrono::milliseconds connectWaitTime = 200ms;

ConnectionHandler::ConnectionHandler():
  m_netioContext{"posix"},
  m_netioThread{&ConnectionHandler::netioMainThread, this}
{}

ConnectionHandler::~ConnectionHandler()
{
  freeConnections();

  m_netioContext.event_loop()->stop();
  m_netioThread.join();
}

//fmt::format("{:02X} ", byte);

void ConnectionHandler::netioMainThread(void)
{
  try
  {
    m_netioContext.event_loop()->run_forever();
  }
  catch (const std::exception& e)
  {
    console->error("From netio run_forever(): {:s}", e.what());
  }
}

void ConnectionHandler::freeConnections(void)
{
  freeReplySockets();
  freeRequestSockets();
}

void ConnectionHandler::freeReplySockets(void)
{
  for(auto& socket : m_replySockets)
    socket.second->free();
}

void ConnectionHandler::freeRequestSockets(void)
{
  for(auto& socket : m_requestSockets)
    socket.second->free();
}

ConnectionInfo ConnectionHandler::getConnection(
  std::string flxHost,
  unsigned int portToGBTx,
  unsigned int portFromGBTx,
  uint32_t elinkID
  )
{
  bool createdSocket = false;
  ConnectionInfo retVal;
  retVal.flxHost = flxHost;
  retVal.portToGBTx = portToGBTx;
  retVal.portFromGBTx = portFromGBTx;
  retVal.elinkID = elinkID;

  std::string requestSocketStr = fmt::format("{:s}-{:#04x}", flxHost, portToGBTx);
  std::string replySocketStr   = fmt::format("{:s}-{:#04x}-{:#04x}", flxHost, portFromGBTx, elinkID);

  auto requestSock = m_requestSockets.find(requestSocketStr);
  if(requestSock != m_requestSockets.end())
  {
    retVal.requestSocket = requestSock->second;
  }
  else
  {
    createdSocket = true;

    std::shared_ptr<ICRequestSocket> tmp = std::make_shared<ICRequestSocket>(&m_netioContext, flxHost, portToGBTx);

    m_requestSockets[requestSocketStr] = tmp;
    retVal.requestSocket = tmp;
  }

  auto replySock = m_replySockets.find(replySocketStr);
  if(replySock != m_replySockets.end())
  {
    retVal.replySocket = replySock->second;
  }
  else
  {
    createdSocket = true;

    std::shared_ptr<ICReplySocket> tmp = std::make_shared<ICReplySocket>(&m_netioContext, flxHost, portFromGBTx, elinkID);

    m_replySockets[replySocketStr] = tmp;
    retVal.replySocket = tmp;
  }

  if(createdSocket)
    std::this_thread::sleep_for(connectWaitTime);

  return retVal;
}

ICSocket::ICSocket(netio::context *context, std::string flxHost):
  m_netioContext{context},
  m_flxHost{flxHost}
{}

ICSocket::~ICSocket()
{
}

ICRequestSocket::ICRequestSocket(
  netio::context *context,
  std::string flxHost,
  unsigned int portToGBTx
  ):
  ICSocket{context, flxHost},
  m_portToGBTx{portToGBTx},
  m_netioRequestSocket{m_netioContext}
{
  connect();
}

ICRequestSocket::~ICRequestSocket()
{
  free();
}

void ICRequestSocket::connect()
{
  if(m_connected)
    return;

  m_netioRequestSocket.connect(netio::endpoint(m_flxHost, m_portToGBTx));

  if(m_netioRequestSocket.is_open()) // test the connection
    m_connected = true;
  else
    throw std::runtime_error("ICRequestSocket::connect() Unable to connect to FELIX endpoint. The felixcore application seems to be down.");

  return;
}

void ICRequestSocket::free()
{
  if(!m_connected)
    return;

  m_netioRequestSocket.disconnect();
  m_connected = false;
}

void ICRequestSocket::sendNetioMessage(
  std::vector<unsigned char>& netioFrame,
  std::shared_ptr<ICReplySocket> replySocket,
  int maxAttempts,
  bool resubscribeOnRetry
  )
{
  if(!connected())
  {
    console->error("Trying to send a message through a request socket that is not connected ({:s}, {:#04x})", m_flxHost, m_portToGBTx);
    throw std::runtime_error("ICRequestSocket::sendNetioMessage(...) Tried to send a message through a request socket that is not connected");
  }

  if(!replySocket->subscribed())
  {
    console->error("Trying to send a message but the reply socket is not subscribed and listening for the reply ({:s}, {:#04x}, {:#04x})", replySocket->flxHost(), replySocket->portFromGBTx(), replySocket->elinkID());
    throw std::runtime_error("ICRequestSocket::sendNetioMessage(...) Tried to send a message but the reply socket is not subscribed and listening for the reply");
  }

  int attempt = 0;
  bool retry  = false;

  do {
    retry       = false;
    replySocket->resetReply();
    if(attempt > 0)
      console->debug("Attempt {0} for E-link {1}", attempt + 1, replySocket->elinkID());

    netio::message message(&netioFrame[0], netioFrame.size());
    m_netioRequestSocket.send(message);

    { // Start a lock block so it synchronizes with the thread processing the replies
      std::unique_lock<std::mutex> lockParameter(replySocket->mtx());

      // Wait for the other thread to notify that there is a reply or timeout
      if(!( replySocket->condVar().wait_for(lockParameter, 1000ms, [&]{return replySocket->hasReply(); }) ))
      {
        console->debug("Reply hasn't come for E-link {0} after 1000 ms", replySocket->elinkID());
        if(++attempt < maxAttempts)
        {
          if(resubscribeOnRetry)
          { // TODO: Is order important?
            replySocket->free();
            reconnect();
            replySocket->connect();
            std::this_thread::sleep_for(connectWaitTime);
          }

          retry = true;
        }
      }
    }

  } while(retry);
}

ICReplySocket::ICReplySocket(
  netio::context *context,
  std::string flxHost,
  unsigned int portFromGBTx,
  uint32_t elinkID
  ):
  ICSocket{context, flxHost},
  m_portFromGBTx{portFromGBTx},
  m_elinkID{elinkID},
  m_netioReplySocket{m_netioContext, [this](netio::endpoint& endpoint, netio::message& message){this->replyHandler(endpoint, message);}}
{
  connect();
}

ICReplySocket::~ICReplySocket()
{
  free();
}

void ICReplySocket::connect()
{
  if(m_subscribed)
    return;

  m_netioReplySocket.subscribe(m_elinkID, netio::endpoint(m_flxHost, m_portFromGBTx));
  m_subscribed = true;
}

void ICReplySocket::free()
{
  if(!m_subscribed)
    return;

  m_netioReplySocket.unsubscribe(m_elinkID, netio::endpoint(m_flxHost, m_portFromGBTx));
  m_subscribed = false;
}

void ICReplySocket::replyHandler(netio::endpoint&, netio::message& message)
{
  // TODO: Reduce the number of locks, put it all in one or maybe two, there is no need for 3

  { // Lock in order to clear the variable holding the reply
    std::unique_lock<std::mutex> lockParameter(m_mtx);
    m_reply.clear();
  }

  // Retrieve the reply from the message and put into a temporary variable
  std::vector<uint8_t> netioFrame (message.data_copy());

  { // Lock in order to fill the reply variable with the reply message
    std::unique_lock<std::mutex> lockParameter(m_mtx);
    m_reply = netioFrame;
  }

  // Below just for debugging, delete it later (Maybe not needed to delete because it is using a trace message)
  std::string out;
  for(auto& byte : netioFrame)
  {
    out += fmt::format("{:02X} ", byte);
  }
  console->trace("Netio reply from host {:s}, port {:#04x}, elink {:#04x}:\n{:s}", m_flxHost, m_portFromGBTx, m_elinkID, out);

  { // Lock in order to set that a reply is available and notify the condition variable
    std::unique_lock<std::mutex> lockParameter(m_mtx);
    m_hasReply = true;
    m_condVar.notify_one();
  }

  return;
}

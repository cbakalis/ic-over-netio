#include "logging.h"
#include "utilities.h"
#include "IChandler.h"
#include "readIcCfg.h"
#include <chrono>
#include <exception>

using namespace std::chrono_literals;

IChandler::IChandler(ConnectionHandler& connHandler, std::string flxHost, unsigned int portToGBTx, unsigned int portFromGBTx, uint32_t elinkID, bool resubscribeValue):
  m_connHandler{connHandler},
  m_maxRetries{2},
  m_resubscribe{resubscribeValue}, //set to true if you want to resubscribe upon failure to receive message
  m_flxHost{flxHost},
  m_portToGBTx{portToGBTx},
  m_portFromGBTx{portFromGBTx},
  m_elinkID{elinkID}
{
}

IChandler::~IChandler()
{
}

void IChandler::sendCfg(std::string fileName)
{
  if(!isFile(fileName))
  {
    console->error("The given file ({:s}) does not exist", fileName);
    throw std::logic_error("File does not exist");
  }

  readICCfg icCfg(fileName);

  ConnectionInfo connInfo = m_connHandler.getConnection(m_flxHost, m_portToGBTx, m_portFromGBTx, m_elinkID);

  felix::base::ToFELIXHeader header;

  auto icNetioFrame = prepareNetioFrame(false, 0, icCfg.data());
  std::vector<unsigned char> netioFrame ( sizeof(header) + icNetioFrame.size());

  header.length = netioFrame.size() - sizeof(header);
  header.reserved = 0;
  header.elinkid = m_elinkID;

  std::copy(
          reinterpret_cast<uint8_t*>(&header),
          reinterpret_cast<uint8_t*>(&header)+sizeof(header),
          netioFrame.begin() );

  memcpy(&netioFrame[sizeof header], icNetioFrame.data(), icNetioFrame.size());
  for(unsigned int i = 0; i < netioFrame.size(); ++i)
    console->trace("{0}: {1:#04x} ({1:d})", i, netioFrame[i]);

  connInfo.requestSocket->sendNetioMessage(netioFrame, connInfo.replySocket, m_maxRetries+1, m_resubscribe);
}

std::vector<uint8_t> IChandler::readCfg()
{
  std::vector<uint8_t> retVal(436);

  ConnectionInfo connInfo = m_connHandler.getConnection(m_flxHost, m_portToGBTx, m_portFromGBTx, m_elinkID);

  felix::base::ToFELIXHeader header;

  auto icNetioFrame = prepareNetioFrame(true, 0, retVal);
  std::vector<unsigned char> netioFrame ( sizeof(header) + icNetioFrame.size());

  header.length = netioFrame.size() - sizeof(header);
  header.reserved = 0;
  header.elinkid = m_elinkID;

  std::copy(
          reinterpret_cast<uint8_t*>(&header),
          reinterpret_cast<uint8_t*>(&header)+sizeof(header),
          netioFrame.begin() );

  memcpy(&netioFrame[sizeof header], icNetioFrame.data(), icNetioFrame.size());
  for(unsigned int i = 0; i < netioFrame.size(); ++i)
    console->trace("{0}: {1:#04x} ({1:d})", i, netioFrame[i]);

  connInfo.requestSocket->sendNetioMessage(netioFrame, connInfo.replySocket, m_maxRetries+1, m_resubscribe);
  auto reply = connInfo.replySocket->getReply();

  for(unsigned int i = 0; i < retVal.size(); ++i)
    retVal[i] = reply[i + 8 + 8]; // Remove Felix header and GBTx header

  return retVal;
}

std::vector<std::pair<uint16_t, uint8_t>> IChandler::readRegs(std::vector<uint16_t> regs)
{
  std::vector<std::pair<uint16_t, uint8_t>> retVal;

  ConnectionInfo connInfo = m_connHandler.getConnection(m_flxHost, m_portToGBTx, m_portFromGBTx, m_elinkID);

  felix::base::ToFELIXHeader header;

  for(unsigned int i = 0; i < regs.size(); ++i)
  {
    std::vector<uint8_t> vector(1);

    auto icNetioFrame = prepareNetioFrame(true, regs[i], vector);
    std::vector<unsigned char> netioFrame ( sizeof(header) + icNetioFrame.size());

    header.length = netioFrame.size() - sizeof(header);
    header.reserved = 0;
    header.elinkid = m_elinkID;

    std::copy(
            reinterpret_cast<uint8_t*>(&header),
            reinterpret_cast<uint8_t*>(&header)+sizeof(header),
            netioFrame.begin() );

    memcpy(&netioFrame[sizeof header], icNetioFrame.data(), icNetioFrame.size());
    for(unsigned int i = 0; i < netioFrame.size(); ++i)
      console->debug("{0}: {1:#04x} ({1:d})", i, netioFrame[i]);

    connInfo.requestSocket->sendNetioMessage(netioFrame, connInfo.replySocket, m_maxRetries+1, m_resubscribe);
    // TODO: is this finished? it seems we are not retrieving the replies with the register values
  }

  return retVal;
}

void IChandler::sendRegs(std::map<uint16_t, std::vector<uint8_t>> regs)
{
  ConnectionInfo connInfo = m_connHandler.getConnection(m_flxHost, m_portToGBTx, m_portFromGBTx, m_elinkID);

  felix::base::ToFELIXHeader header;

  for(auto& reg : regs)
  {
    auto icNetioFrame = prepareNetioFrame(false, reg.first, reg.second);
    std::vector<unsigned char> netioFrame ( sizeof(header) + icNetioFrame.size());

    header.length = netioFrame.size() - sizeof(header);
    header.reserved = 0;
    header.elinkid = m_elinkID;

    std::copy(
            reinterpret_cast<uint8_t*>(&header),
            reinterpret_cast<uint8_t*>(&header)+sizeof(header),
            netioFrame.begin() );

    memcpy(&netioFrame[sizeof header], icNetioFrame.data(), icNetioFrame.size());
    for(unsigned int i = 0; i < netioFrame.size(); ++i)
      console->debug("{0}: {1:#04x} ({1:d})", i, netioFrame[i]);

    connInfo.requestSocket->sendNetioMessage(netioFrame, connInfo.replySocket, m_maxRetries+1, m_resubscribe);
  }
}

void IChandler::setMaxRetries(int val)
{
  if(val >= 0)
    m_maxRetries = val;
  else
    m_maxRetries = 2;
}


CMAKE_MINIMUM_REQUIRED(VERSION 2.6 FATAL_ERROR)

#SET(CMAKE_VERBOSE_MAKEFILE on)

# CMake generation starts here
PROJECT(IC_OVER_NETIO)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

SET(SOFTWARE_PATH "" CACHE STRING "Path pointing to the felix software directory")

if(SOFTWARE_PATH STREQUAL "")
  MESSAGE(FATAL_ERROR "Please set the Felix software path with the option -DSOFTWARE_PATH to cmake\nEg: cmake -DSOFTWARE_PATH [PATH] ..")
endif()

ADD_CUSTOM_TARGET(link_target ALL
                  COMMAND ${CMAKE_COMMAND} -E create_symlink ${SOFTWARE_PATH}/external ${CMAKE_CURRENT_SOURCE_DIR}/external)

LIST(APPEND CMAKE_MODULE_PATH ${SOFTWARE_PATH}/cmake_tdaq/cmake/modules)

include(FELIX)

include_directories( $ENV{FELIX_ROOT}/include )
link_directories( $ENV{FELIX_ROOT}/lib )

felix_add_external(spdlog ${spdlog_version})
felix_add_external(json ${json_version})

get_property(dirs DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY INCLUDE_DIRECTORIES)
foreach(dir ${dirs})
  message(STATUS "dir='${dir}'")
endforeach()

LIST(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)

INCLUDE_DIRECTORIES(
    $ENV{FELIX_ROOT}/include
    ${PROJECT_SOURCE_DIR}/include
)

FILE(GLOB sources ${PROJECT_SOURCE_DIR}/lib/*.cc)
FILE(GLOB headers ${PROJECT_SOURCE_DIR}/include/*.h)

MESSAGE("Sources: ${sources}")
MESSAGE("Headers: ${headers}")

ADD_LIBRARY(ic_comms STATIC ${sources} ${headers})
ADD_DEPENDENCIES(ic_comms link_target)

ADD_EXECUTABLE(ic_over_netio src/ic_over_netio.cc)
TARGET_LINK_LIBRARIES(ic_over_netio ic_comms netio -lpthread)

ADD_EXECUTABLE(multi_gbtx_ic src/multi_gbtx_ic.cc)
TARGET_LINK_LIBRARIES(multi_gbtx_ic ic_comms netio -lpthread)

#include <iostream>
#include <vector>
#include <algorithm>
#include <thread>

#include <netio.hpp>
#include <felixbase/client.hpp>

#include "logging.h"
#include "readIcCfg.h"
#include "utilities.h"
#include "IChandler.h"

#include "cxxopts.hpp"

int main(int argc, char** argv)
{
  cxxopts::Options options(argv[0], "A utilitiy to configure/interact with the IC channel over Netio");

  options.add_options()
    ("h,help", "Print this help message", cxxopts::value<bool>())
    ("d,debug", "Enable debug (and verbose) output", cxxopts::value<bool>())
  ;

  std::string felixHostname = "pcatlnswfelix01.cern.ch";
  unsigned int portToGBTx = 12340;
  unsigned int portFromGBTx = 12350;
  uint32_t elinkId = 0x3e;
  options.add_options("Configuration")
    ("f,felixHostname", fmt::format("The hostname of the machine the GBTx is connected to. Default: {:s}", felixHostname), cxxopts::value<std::string>())
    ("t,portToGBTx", fmt::format("The port to which to send data to the GBTx. Default {:d}", portToGBTx), cxxopts::value<uint>())
    ("r,portFromGBTx", fmt::format("The port from which to receive data from the GBTx. Default: {:d}", portFromGBTx), cxxopts::value<uint>())
    ("e,eLinkID", fmt::format("The elinkID with which to interact on the host machine. Receives hexadecimal (0xnnnn), ocatal (0nnnnnn) or decimal (nnnnn) Default: {:#04x}", elinkId), cxxopts::value<std::string>())
  ;

  bool read = false;
  std::vector<uint16_t> readAddresses = {};
  options.add_options("READ")
    ("read", "Read and print the configuration", cxxopts::value<bool>())
    ("registers", "If specified, will define the addresses of the registers to be read. The registers can "
     "be passed as a list (eg: --registers 20,24,1,50) no spaces are allowed inside the list. The addresses are given in decimal", cxxopts::value<std::vector<int>>())
  ;

  std::string configFileName;
  std::map<uint16_t, std::vector<uint8_t>> addrValue;
  options.add_options("WRITE")
    ("c,config", "GBTx configuration file", cxxopts::value<std::string>())
    ("a,addresses", "Adresses and values to be written to. Specify the first address, then the length "
     "and then the value to be written into those addresses. Write more triplets for more addresses to "
     "be written to. (e.g: write to 102 3 sequential values of ff and 204 2 sequential values of c4 --addresses 102,3,ff,204,2,c4)"
     ". The values are assumed to be passed in hexadecimal and must be a single byte, the addresses are assumed to be in decimal.",
     cxxopts::value<std::vector<std::string>>())
  ;

  try
  {
    auto result = options.parse(argc, argv);

    if(result.count("debug") > 0)
    {
      spdlog::set_level(spdlog::level::debug);
    }

    if(result.count("help") > 0)
    {
      std::cout << options.help() << std::endl;
      return 0;
    }

    if(result.count("felixHostname") > 0)
    {
      if(result.count("felixHostname") == 1)
        felixHostname = result["felixHostname"].as<std::string>();
      else
      {
        console->error("Please define only one FELIX hostname");
        return 1;
      }
    }

    if(result.count("portToGBTx") > 0)
    {
      if(result.count("portToGBTx") == 1)
        portToGBTx = result["portToGBTx"].as<uint>();
      else
      {
        console->error("Please define only one port to the GBTx");
        return 1;
      }
    }

    if(result.count("portFromGBTx") > 0)
    {
      if(result.count("portFromGBTx") == 1)
        portFromGBTx = result["portFromGBTx"].as<uint>();
      else
      {
        console->error("Please define only one port from the GBTx");
        return 1;
      }
    }

    if(result.count("eLinkID") > 0)
    {
      if(result.count("eLinkID") == 1)
      {
        std::string tmp = result["eLinkID"].as<std::string>();
        try{
          elinkId = std::stoul(tmp, 0, 0); // stoul automatically infers the base: 0 -> octal, 0x -> Hex, else decimal
          std::string tmp2;
          if(tmp[0] == '0')
          {
            if(tmp[1] == 'x')
              tmp2 = fmt::format("{:#x}", elinkId);
            else
            {
              if(tmp[1] == 'X')
                tmp2 = fmt::format("{:#X}", elinkId);
              else
                tmp2 = fmt::format("{:#o}", elinkId);
            }
          }
          else
            tmp2 = fmt::format("{:d}", elinkId);

          if(tmp != tmp2)
            throw std::exception();
        }
        catch(...)
        {
          console->error("Please give a valid e-link ID");
          std::cout << options.help() << std::endl;
          throw;
        }
      }
      else
      {
        console->error("Please define only one e-link ID");
        return 1;
      }
    }

    if(result.count("read") > 0)
      read = true;
    if(result.count("registers") > 0)
    {
      if(result.count("registers") > 1)
      {
        console->error("Please define only one list of registers to be read");
        return 1;
      }
      auto tmp = result["registers"].as<std::vector<int>>();
      for(auto &addr : tmp)
        readAddresses.push_back(addr);
    }

    if(result.count("config") > 0)
    {
      if(result.count("config") > 1)
      {
        console->error("Please define only one configuration file");
        return 1;
      }
      configFileName = result["config"].as<std::string>();
      if(!isFile(configFileName))
      {
        console->error("Please define an existing configuration file");
        return 1;
      }
    }

    if(result.count("addresses") > 0)
    {
      if(result.count("addresses") > 1)
      {
        console->error("Please define only one set of addresses");
        return 1;
      }
      console->debug("Reading the addresses to write to");

      std::vector<std::string> tmp = result["addresses"].as<std::vector<std::string>>();
      console->debug("Got a list with {} entries", tmp.size());

      if(tmp.size()%3 != 0)
      {
        console->error("You must define triplets of values (Address, length, value)");
        std::cout << options.help() << std::endl;
        return 1;
      }

      for(size_t i = 0; i < tmp.size()/3; ++i)
      {
        console->debug("Going through set {}", i);
        uint8_t value = std::stoul(tmp[i*3 + 2], 0, 16);
        std::vector<uint8_t> values;

        for(size_t j = 0; j < std::stoul(tmp[i*3 + 1], 0, 10); ++j)
          values.push_back(value);

        addrValue[std::stoul(tmp[i*3], 0, 10)] = values;
      }
    }

    if(read == false && (configFileName == "" && addrValue.size() == 0))
    {
      console->error("You must either read or write to the GBTx");
      std::cout << options.help() << std::endl;
      return 1;
    }
  }
  catch(...)
  {
    std::cout << options.help() << std::endl;
    return 1;
  }

  console->debug("The felix hostname is: {:s}", felixHostname);
  console->debug("The port to the GBTx is: {:d}", portToGBTx);
  console->debug("The port from the GBTx is: {:d}", portFromGBTx);
  console->debug("The e-link ID is: {:#04x}", elinkId);


  try
  {
    ConnectionHandler connections;
    IChandler var(connections, felixHostname, portToGBTx, portFromGBTx, elinkId);

    if(read)
      {
	if(readAddresses.size() == 0)
	  {
	    console->debug("Reading the full configuration");
	    auto cfg = var.readCfg();

	    std::string tmp;
	    for(size_t i = 0; i < cfg.size(); ++i)
	      {
		if(i % 16 == 0)
		  {
		    if(i != 0)
		      std::cout << tmp << std::endl;
		    tmp = fmt::format("{:>4d}: ", i);
		  }

		tmp += fmt::format(" {:02x}", cfg[i]);
	      }
	    std::cout << tmp << std::endl;
	  }
	else
	  {
	    console->debug("Reading the specific register addresses");
	    auto regs = var.readRegs(readAddresses);

	    for(auto& addr : regs)
	      {
		std::cout << fmt::format("{:>4d}", addr.first) << ": " << fmt::format("{:02x}", addr.second) << std::endl;
	      }
	  }
      }
    else
      {
	if(configFileName != "")
	  {
	    console->debug("The config file is: ", configFileName);
	    var.sendCfg(configFileName);
	  }
	else
	  {
	    console->debug("Writing to the following sets of addresses:");
	    for(auto& entry : addrValue)
	      {
		std::string tmp = fmt::format("   - {}:", entry.first);
		for(auto& value : entry.second)
		  tmp += fmt::format(" {:x}", value);

		console->debug(tmp);
	      }

	    var.sendRegs(addrValue);
	  }
      }
  }
  catch(std::runtime_error& e)
  {
    console->debug(e.what());
    return 20;
  }

  return 0;
}

#include <iostream>
#include <vector>
#include <algorithm>
#include <thread>
#include <fstream>
#include <exception>

#include <netio.hpp>
#include <felixbase/client.hpp>

#include "logging.h"
#include "readIcCfg.h"
#include "utilities.h"
#include "IChandler.h"
#include "connectionHandler.h"

#include "cxxopts.hpp"
#include "json.hpp"

using json = nlohmann::json;

int main(int argc, char** argv)
{
  cxxopts::Options options(argv[0], "A utilitiy to configure/interact with the IC channel over Netio");
  int numRetries = 0;
  bool resubscribeValue = false;

  options.add_options()
    ("h,help",    "Print this help message", cxxopts::value<bool>())
    ("d,debug",   "Enable debug (and verbose) output", cxxopts::value<bool>())
    ("t,trace",   "Enable trace (and verbose) output", cxxopts::value<bool>())
    ("r,retries", "Set maximum number of retries upon failure of receiving reply from FELIX", cxxopts::value<int>(numRetries)->default_value("2"))
    ("resubscribe", "Set if you want to resubscribe upon failure of receiving reply from FELIX", cxxopts::value<bool>(resubscribeValue))

    //resubscribeValue
  ;

  std::string configFileName;
  options.add_options("Configuration")
    ("c,config", "Configuration file describing which GBTx to connect to and their default configuration files. See exampleCfg.json", cxxopts::value<std::string>())
  ;

  //bool read = false;
  //std::vector<uint16_t> readAddresses = {};
  //options.add_options("READ")
  //  ("read", "Read and print the configuration", cxxopts::value<bool>())
  //  ("registers", "If specified, will define the addresses of the registers to be read. The registers can "
  //   "be passed as a list (eg: --registers 20,24,1,50) no spaces are allowed inside the list. The addresses are given in decimal", cxxopts::value<std::vector<int>>())
  //;

  std::map<uint16_t, std::vector<uint8_t>> addrValue;
  options.add_options("WRITE")
    ("a,addresses", "Adresses and values to be written to. Specify the first address, then the length "
     "and then the value to be written into those addresses. Write more triplets for more addresses to "
     "be written to. (e.g: write to 102 3 sequential values of ff and 204 2 sequential values of c4 --addresses 102,3,ff,204,2,c4)"
     ". The values are assumed to be passed in hexadecimal and must be a single byte, the addresses are assumed to be in decimal.",
     cxxopts::value<std::vector<std::string>>())
  ;

  try
  {
    auto result = options.parse(argc, argv);

    if(result.count("debug") > 0)
    {
      spdlog::set_level(spdlog::level::debug);
    }

    if(result.count("trace") > 0)
    {
      spdlog::set_level(spdlog::level::trace);
    }

    if(result.count("help") > 0)
    {
      std::cout << options.help() << std::endl;
      return 0;
    }

    if(numRetries < 0)
    {
      std::cout << "Number of retries cannot be a negative number. Using zero instead." << std::endl;
      numRetries = 0;
    }

    //if(result.count("read") > 0)
    //  read = true;
    //if(result.count("registers") > 0)
    //{
    //  if(result.count("registers") > 1)
    //  {
    //    console->error("Please define only one list of registers to be read");
    //    return 1;
    //  }
    //  auto tmp = result["registers"].as<std::vector<int>>();
    //  for(auto &addr : tmp)
    //    readAddresses.push_back(addr);
    //}

    if(result.count("config") > 0)
    {
      if(result.count("config") > 1)
      {
        console->error("Please define only one configuration file");
        return 1;
      }
      configFileName = result["config"].as<std::string>();
    }

    if(result.count("addresses") > 0)
    {
      if(result.count("addresses") > 1)
      {
        console->error("Please define only one set of addresses");
        return 1;
      }
      console->debug("Reading the addresses to write to");

      std::vector<std::string> tmp = result["addresses"].as<std::vector<std::string>>();
      console->debug("Got a list with {} entries", tmp.size());

      if(tmp.size()%3 != 0)
      {
        console->error("You must define triplets of values (Address, length, value)");
        return 1;
      }

      for(size_t i = 0; i < tmp.size()/3; ++i)
      {
        console->debug("Going through set {}", i);
        uint8_t value = std::stoul(tmp[i*3 + 2], 0, 16);
        std::vector<uint8_t> values;

        for(size_t j = 0; j < std::stoul(tmp[i*3 + 1], 0, 10); ++j)
          values.push_back(value);

        addrValue[std::stoul(tmp[i*3], 0, 10)] = values;
      }
    }

    //if(read == false && (configFileName == "" && addrValue.size() == 0))
    //{
    //  console->error("You must either read or write to the GBTx");
    //  return 1;
    //}
    if(!isFile(configFileName))
    {
      console->error("Please give a valid configuration file");
      return 1;
    }
  }
  catch(...)
  {
    std::cout << options.help() << std::endl;
    return 1;
  }

  json inJson;
  std::ifstream inFile(configFileName);
  inFile >> inJson;

  if(inJson.find("configurations") == inJson.end())
  {
    console->error("Please give a valid configuration file");
    return 1;
  }

  ConnectionHandler connections;

  for(auto & type : inJson["configurations"])
  {
    std::string  t_configFile = "";
    std::string  t_felixHostname = "";
    unsigned int t_portToGBTx = 0;
    unsigned int t_portFromGBTx = 0;
    std::string  t_elinkID = "0xffffffff";

    if(type.find("configFile") != type.end())
      t_configFile = type["configFile"];
    if(type.find("hostname") != type.end())
      t_felixHostname = type["hostname"];
    if(type.find("portToGBTx") != type.end())
      t_portToGBTx = type["portToGBTx"];
    if(type.find("portFromGBTx") != type.end())
      t_portFromGBTx = type["portFromGBTx"];
    if(type.find("elinkID") != type.end())
      t_elinkID = type["elinkID"];

    if(type.find("hosts") == type.end())
      continue;

    for(auto& host : type["hosts"].items())
    {
      std::string  h_configFile = t_configFile;
      std::string  h_felixHostname = host.key();
      unsigned int h_portToGBTx = t_portToGBTx;
      unsigned int h_portFromGBTx = t_portFromGBTx;
      std::string  h_elinkID = t_elinkID;

      auto h_values = host.value();

      if(h_values.find("configFile") != h_values.end())
        h_configFile = h_values["configFile"];
      if(h_values.find("hostname") != h_values.end())
        h_felixHostname = h_values["hostname"];
      if(h_values.find("portToGBTx") != h_values.end())
        h_portToGBTx = h_values["portToGBTx"];
      if(h_values.find("portFromGBTx") != h_values.end())
        h_portFromGBTx = h_values["portFromGBTx"];
      if(h_values.find("elinkID") != h_values.end())
        h_elinkID = h_values["elinkID"];

      for(auto& l1ddc : h_values.items())
      {
        if(
           l1ddc.key() == "configFile"   ||
           l1ddc.key() == "hostname"     ||
           l1ddc.key() == "portToGBTx"   ||
           l1ddc.key() == "portFromGBTx" ||
           l1ddc.key() == "elinkID"
          )
          continue;

        std::string  configFile = h_configFile;
        std::string  felixHostname = h_felixHostname;
        unsigned int portToGBTx = h_portToGBTx;
        unsigned int portFromGBTx = h_portFromGBTx;
        std::string  elinkID = h_elinkID;

        auto values = l1ddc.value();

        if(values.find("configFile") != values.end())
          configFile = values["configFile"];
        if(values.find("hostname") != values.end())
          felixHostname = values["hostname"];
        if(values.find("portToGBTx") != values.end())
          portToGBTx = values["portToGBTx"];
        if(values.find("portFromGBTx") != values.end())
          portFromGBTx = values["portFromGBTx"];
        if(values.find("elinkID") != values.end())
          elinkID = values["elinkID"];

        try
        {
          IChandler var(connections, felixHostname, portToGBTx, portFromGBTx, std::stoul(elinkID, 0, 0), resubscribeValue);
          var.setMaxRetries(numRetries);

          if(addrValue.size() == 0)
          {
	          console->trace("The config file is: {:s}", configFileName);
	          var.sendCfg(configFile);
    	    }
          else
    	    {
	          console->debug("Writing to the following sets of addresses:");
	          for(auto& entry : addrValue)
      	    {
        		  std::string tmp = fmt::format("   - {}:", entry.first);
        		  for(auto& value : entry.second)
        		    tmp += fmt::format(" {:x}", value);

        		  console->debug(tmp);
        		}

    	      var.sendRegs(addrValue);
    	    }
      	}
      	catch (std::runtime_error& e)
      	{
      	  console->debug(e.what());
      	  return 20;
        }
      }
    }
  }


  return 0;
}

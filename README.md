## General Information
Project has started as of 31/07/2019

see https://its.cern.ch/jira/browse/ATLNSWDAQ-51

also https://espace.cern.ch/GBT-Project/GBTX/Manuals/gbtxManual.pdf

## Compilation
Only compiles and runs on a machine with FELIX installed.
The Felix software must also be installed

  * source tdaq_cmake (must always be done at start of session): `source software/cmake_tdaq/bin/setup.sh x86_64-centos7-gcc8-opt`

  * Create a build directory for IC-over-netio: `mkdir [buildDir]`

  * Change to the build directory `cd [buildDir]`

  * Run cmake: `cmake -DSOFTWARE_PATH=[pathToFLXSft] [srcDir]`

  * Run make: `make`

## Installing FLX software
Follow instructions on:
https://gitlab.cern.ch/atlas-tdaq-felix/software


## Acknowledgments
Special thanks to OPC-UA and FELIX developers.

Using code from https://gitlab.cern.ch/atlas-dcs-common-software/
and https://gitlab.cern.ch/atlas-tdaq-felix/ftools
